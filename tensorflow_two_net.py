import tensorflow as tf
import numpy as np

# create data
x_data = np.random.rand(100).astype(np.float32)
y_data = x_data*0.1+0.3

###creat tensorflow structure start###
Weights = tf.Variable(tf.random_uniform([1],-1.0,1.0))
biases = tf.Variable(tf.zeros([1]))
y = Weights*x_data + biases
loss = tf.reduce_mean(tf.square(y-y_data))
optimizer = tf.train.GradientDescentOptimizer(0.5)  #learning rate
train = optimizer.minimize(loss)
init = tf.initialize_all_variables()

###creat tensorflow structure end###
sess = tf.Session()
sess.run(init)     #Very important

"If you want to creat another neural net,creat a new session named sess2"
sess2 = tf.Session()   
sess2.run(init)

for step in range(201):
    sess.run(train)
    
    """
    Then run it with the same model, if you want to change to different model,
    feel free to replace train with the one whatever you want."
    """
    sess2.run(train)
    if step % 20 ==0:
        print(step,sess.run(Weights),sess.run(biases))
        print(step,sess2.run(Weights),sess2.run(biases))