import tensorflow as tf

input1 = tf.placeholder(tf.float32)
input2 = tf.placeholder(tf.float32)

adder_node = input1 + input2

add_and_triple = adder_node * 3.

tf_multiply = tf.multiply(input1, input2)

print('adder_node result:')
with tf.Session() as session:
	print(session.run(adder_node, feed_dict={input1:[2.], input2:[7.]}))
	print(session.run(adder_node, feed_dict={input1:[10], input2:[5.5, 6.6]}))
	print(session.run(adder_node, feed_dict={input1:[10.11111, 20.22222], input2:[7]}))
	
print('add_and_triple result:')
with tf.Session() as session:
	print(session.run(add_and_triple, feed_dict={input1:[2.], input2:[7.]}))
	print(session.run(add_and_triple, feed_dict={input1:[10], input2:[5.5, 6.6]}))
	print(session.run(add_and_triple, feed_dict={input1:[10.11111, 20.22222], input2:[7]}))

print('tf_multiply result:')
with tf.Session() as session:
	print(session.run(tf_multiply, feed_dict={input1:[2.], input2:[7.]}))
	print(session.run(tf_multiply, feed_dict={input1:[10], input2:[5.5, 6.6]}))
	print(session.run(tf_multiply, feed_dict={input1:[10.11111, 20.22222], input2:[7]}))
