from tensorflow.examples.tutorials.mnist import input_data
import tensorflow as tf

def add_layer(inputs, input_size, output_size, activation_function = None):
	Weights = tf.Variable(tf.random_normal([input_size, output_size]))
	biases = tf.Variable(tf.zeros([1, output_size]) + 0.1)
	Wx_plus_b = tf.matmul(inputs, Weights) + biases
	
	if activation_function is None:
		outputs = Wx_plus_b
	else:
		outputs = activation_function(Wx_plus_b)
		
	return outputs
	
	
def compute_accuracy(v_xs, v_ys):
	global prediction
	
	y_pre = session.run(prediction, feed_dict={xs:v_xs})
	
	correct_prediction = tf.equal(tf.argmax(y_pre, 1), tf.argmax(v_ys, 1))
	accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
	
	result = session.run(accuracy, feed_dict={xs:v_xs,ys:v_ys})
	
	return result
	

mnist = input_data.read_data_sets('MNIST_data', one_hot=True)

#define placeholder for inputs to network
xs = tf.placeholder(tf.float32, [None, 784]) # 28x28
ys = tf.placeholder(tf.float32, [None, 10])

#add output layer
prediction = add_layer(xs, 784, 10, activation_function=tf.nn.softmax)

#the error between prediction and real data
cross_entropy = tf.reduce_mean(-tf.reduce_sum(ys * tf.log(prediction), reduction_indices=[1])) #loss
train_step = tf.train.GradientDescentOptimizer(0.5).minimize(cross_entropy)

session = tf.Session()

session.run(tf.global_variables_initializer())

for i in range(1000):
	batch_xs, batch_ys = mnist.train.next_batch(100)

	session.run(train_step, feed_dict={xs:batch_xs,ys:batch_ys})
	
	if i % 50 == 0:
		print(compute_accuracy(mnist.test.images, mnist.test.labels))