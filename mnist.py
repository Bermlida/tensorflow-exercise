from tensorflow.examples.tutorials.mnist import input_data
import tensorflow as tf

mnist = input_data.read_data_sets("MNIST_data/", one_hot=True)

x = tf.placeholder(tf.float32, [None, 784])

# define weight and baise
Weight = tf.Variable(tf.zeros([784, 10]))
baise = tf.Variable(tf.zeros([10]))

# define prediction and desired outcome
prediction = tf.nn.softmax(tf.matmul(x, Weight) + baise)
desired_outcome = tf.placeholder(tf.float32, [None, 10])

# define loss function
#cross_entropy = tf.reduce_mean(-tf.reduce_sum(desired_outcome * tf.log(prediction), reduction_indices=[1]))
cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=desired_outcome, logits=prediction))

# define train
train_step = tf.train.GradientDescentOptimizer(0.05).minimize(cross_entropy)


session = tf.InteractiveSession()

tf.global_variables_initializer().run()

for _ in range(1000):
  batch_xs, batch_ys = mnist.train.next_batch(100)
  session.run(train_step, feed_dict={x: batch_xs, desired_outcome: batch_ys})
  
correct_prediction = tf.equal(tf.argmax(prediction, 1), tf.argmax(desired_outcome, 1))

accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

print(session.run(accuracy, feed_dict={x: mnist.test.images, desired_outcome: mnist.test.labels}))