import tensorflow as tf

state = tf.Variable(0, name='counter')
#print(state.name)

one = tf.constant(1)
#print(one)

new_state = tf.add(state, one)

update = tf.assign(state, new_state)

with tf.Session() as session:
	session.run(tf.global_variables_initializer())
	
	for _ in range(3):
		session.run(update)
		print(session.run(state))