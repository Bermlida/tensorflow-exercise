import tensorflow as tf

matrix1 = tf.constant([[3, 3]])
matrix2 = tf.constant([[2], [2]])

product = tf.matmul(matrix1, matrix2)

session1 = tf.Session()
result1 = session1.run(product)
print "method1: %r" %(result1)
session1.close()

with tf.Session() as session2:
	result2 = session2.run(product)
	print "method2: %r" %(result2)