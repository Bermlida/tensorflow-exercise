import tensorflow as tf

def loss_function(model_value, prodived_value):
	error_deltas = model_value - prodived_value
	return tf.reduce_sum(tf.square(error_deltas))

#W = tf.constant([.3], dtype=tf.float32) # weight
#b = tf.constant([-.3], dtype=tf.float32) # biase

W = tf.Variable([.3], dtype=tf.float32) # weight
b = tf.Variable([-.3], dtype=tf.float32) # biase

x = tf.placeholder(tf.float32) #trainable_parameter
y = tf.placeholder(tf.float32) #prodived_data

x_data = [1,2,3,4]
y_data = [0,-1,-2,-3]

linear_model = W * x + b #train model

loss = loss_function(linear_model, y) #loss value

# manually adjust weight and biases
fixW = tf.assign(W, [-1.]) # weight for after fix
fixb = tf.assign(b, [1.]) # biase for after fix

# automatically adjust weight and biases by optimizer
optimizer = tf.train.GradientDescentOptimizer(0.01)
train = optimizer.minimize(loss)

# not use optimizer
with tf.Session() as session:
	session.run(tf.global_variables_initializer())
	
	print('---not use optimizer---')
	
	curr_W, curr_b, curr_loss = session.run([W, b, loss], {x:x_data, y:y_data})
	print('(before fix)weight:{0}, biase:{1}, loss:{2}' . format(curr_W, curr_b, curr_loss))
	
	session.run([fixW, fixb])
	curr_W, curr_b, curr_loss = session.run([W, b, loss], {x:x_data, y:y_data})
	print('(after fix)weight:{0}, biase:{1}, loss:{2}' . format(curr_W, curr_b, curr_loss))

	
# use optimizer
with tf.Session() as session:
	session.run(tf.global_variables_initializer())
	
	print('---use optimizer---')
	
	curr_W, curr_b, curr_loss = session.run([W, b, loss], {x:x_data, y:y_data})
	print('(before fix)weight:{0}, biase:{1}, loss:{2}' . format(curr_W, curr_b, curr_loss))
	
	for i in range(1000):
  		session.run(train, {x:[1,2,3,4], y:[0,-1,-2,-3]})
		
	curr_W, curr_b, curr_loss = session.run([W, b, loss], {x:x_data, y:y_data})
	print('(after fix)weight:{0}, biase:{1}, loss:{2}' . format(curr_W, curr_b, curr_loss))